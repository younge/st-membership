WITH 
T1 AS 
(
SELECT DISTINCT EventJobId
FROM temporary.memevent ME
)
,
DS1 AS 
(
select 
J.Customer_Name
,J.Customer
,J.Job_Id
,J.Location_Id
,J.Job_Number
,J.Jobsite_Id
,EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) as Start_Date
,J.Duration
,J.Type_Id
,J.Type
,J.Status
,J.Modified
,ME.eventdate
,ME.eventId
,ME.RecurringServiceType
from data_warehouse.Job J
JOIN temporary.memevent ME on J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id 
and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2018-03-01' and '2018-09-30'
and cast(ME.eventdate as date) between '2018-02-01' and '2018-08-01'
and ME.eventJobNumber is null
LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

WHERE 
T1.EventJobId is null 
and 
Type_id IN 
(
15830027   --Inspection - Cooling 
,15830028   --Inspection - Heating
,162318827   --Inspection (1) System
,162320866   --Inspection (2) System
,162322224   --Inspection (3) Systems
,162323246   --Inspection (4) Systems
,162323127   --Inspection (5) Systems
,162319967   --Inspection (6) Systems
,163255256   --Inspection (8) Systems
)
and J.Status = 'Completed'
and ME.RecurringServiceType in
  (
    'BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection  Cooling 1 System','Inspection  Cooling 1 System Cont'
,'Inspection  Cooling 2 System','Inspection  Cooling 2 System Cont','Inspection  Cooling 3 System','Inspection  Cooling 4 System','Inspection  Cooling 5 System'
,'Inspection  Cooling 6 System','Inspection  Heating 1 System','Inspection  Heating 1 System Cont','Inspection  Heating 2 System','Inspection  Heating 2 System Cont'
,'Inspection  Heating 3 System''Inspection  Heating 4 System','Inspection  Heating 5 System','Inspection  Heating 6 System','Inspection 1 System','Inspection 1 System Cont'
,'Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont','Inspection 4 System','Inspection 4 System Cont','Inspection 5 System'
,'Inspection 5 System Cont','Inspection 6 System','Inspection 7 System','Inspection 8 System'
  )


),
DS2 AS 
(
select 
J.Customer_Name
,J.Customer
,J.Job_Id
,J.Location_Id
,J.Job_Number
,J.Jobsite_Id
,EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) as Start_Date
,J.Duration
,J.Type_Id
,J.Type
,J.Status
,J.Modified
,ME.eventdate
,ME.eventId
,ME.RecurringServiceType
from data_warehouse.Job J
JOIN temporary.memevent ME on J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id 
and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2018-10-01' and '2019-02-28'
and cast(ME.eventdate as date) between '2018-09-01' and '2019-01-31'
and ME.eventJobNumber is null
LEFT JOIN T1 ON J.Job_Id = T1.EventJobId
LEFT JOIN DS1 ON J.Job_Id = DS1.Job_Id
WHERE 
T1.EventJobId is null 
AND DS1.Job_Id IS NULL
and 
Type_id IN 
(
15830027   --Inspection - Cooling 
,15830028   --Inspection - Heating
,162318827   --Inspection (1) System
,162320866   --Inspection (2) System
,162322224   --Inspection (3) Systems
,162323246   --Inspection (4) Systems
,162323127   --Inspection (5) Systems
,162319967   --Inspection (6) Systems
,163255256   --Inspection (8) Systems
)
and J.Status = 'Completed'
and ME.RecurringServiceType in
  (
    'BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection  Cooling 1 System','Inspection  Cooling 1 System Cont'
,'Inspection  Cooling 2 System','Inspection  Cooling 2 System Cont','Inspection  Cooling 3 System','Inspection  Cooling 4 System','Inspection  Cooling 5 System'
,'Inspection  Cooling 6 System','Inspection  Heating 1 System','Inspection  Heating 1 System Cont','Inspection  Heating 2 System','Inspection  Heating 2 System Cont'
,'Inspection  Heating 3 System''Inspection  Heating 4 System','Inspection  Heating 5 System','Inspection  Heating 6 System','Inspection 1 System','Inspection 1 System Cont'
,'Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont','Inspection 4 System','Inspection 4 System Cont','Inspection 5 System'
,'Inspection 5 System Cont','Inspection 6 System','Inspection 7 System','Inspection 8 System'
  )
),
DS3 AS 
(
select 
J.Customer_Name
,J.Customer
,J.Job_Id
,J.Location_Id
,J.Job_Number
,J.Jobsite_Id
,EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) as Start_Date
,J.Duration
,J.Type_Id
,J.Type
,J.Status
,J.Modified
,ME.eventdate
,ME.eventId
,ME.RecurringServiceType
from data_warehouse.Job J
JOIN temporary.memevent ME on J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id 
and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2018-10-01' and '2019-02-28'
and cast(ME.eventdate as date) between '2018-09-01' and '2019-01-31'
and ME.eventJobNumber is null
LEFT JOIN T1 ON J.Job_Id = T1.EventJobId
LEFT JOIN DS1 ON J.Job_Id = DS1.Job_Id
WHERE 
T1.EventJobId is null 
AND DS1.Job_Id IS NULL
and 
Type_id IN 
(
15830030   --Inspection - Plumbing
,162319973   --Inspection (1) Water Heaters
,162322979   --Inspection (2) Water Heater
,162319364   --Inspection (3) Water Heaters
,162320882   --Inspection (4) Water Heaters

)
and J.Status = 'Completed'
and ME.RecurringServiceType in
  (
    'BOGO 1 Water Heater','BOGO 2 Water Heater','Inspection  Plumbing 1 Water Heater','Inspection  Plumbing 1 Water Heater Cont','Inspection  Plumbing 2 Water Heater'
,'Inspection  Plumbing 2 Water Heater Cont','Inspection  Plumbing 3 Water Heater','Inspection  Plumbing 3 Water Heater Cont','Inspection  Plumbing 4 Water Heater'
,'Plumbing 1 Water Heater','Plumbing 1 Water Heater Cont','Plumbing 2 Water Heater','Plumbing 2 Water Heater Cont','Plumbing 3 Water Heater'
,'Plumbing 3 Water Heater Cont','Plumbing 4 Water Heater'
  )
)



 SELECT *
 FROM DS1
 UNION ALL
SELECT *
 FROM DS2
 UNION ALL
SELECT *
 FROM DS3
 UNION ALL
 order by DS1.Customer