WITH
  T1
  AS
  (
    SELECT DISTINCT EventJobId
    FROM data_warehouse_staging.memevent ME
  ),
  TIME1
  as
  (
    select
   J.Job_Id
    from data_warehouse.Job J
  CROSS JOIN data_warehouse_staging.memevent ME
  LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

    where
      J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id
      /*Changed the date from 2018-03-01 to 2018-02-01. Ask RaShanda if that is ok. Seems like we should be matching jobs as soon as we turned on the system. EY 05/14/2019*/
      AND EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2018-02-01' and '2018-09-30'
      and cast(ME.eventdate as date) between '2018-02-01' and '2018-08-01'
      AND T1.EventJobId is null
      and ME.eventJobNumber is null
      AND Type_id IN 
(
15830027   --Inspection - Cooling 
,15830028   --Inspection - Heating
,162318827   --Inspection (1) System
,162320866   --Inspection (2) System
,162322224   --Inspection (3) Systems
,162323246   --Inspection (4) Systems
,162323127   --Inspection (5) Systems
,162319967   --Inspection (6) Systems
,163255256   --Inspection (8) Systems
)
      and J.Status = 'Completed'
      and ME.RecurringServiceType in
  (
    'BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection  Cooling 1 System','Inspection  Cooling 1 System Cont'
,'Inspection  Cooling 2 System','Inspection  Cooling 2 System Cont','Inspection  Cooling 3 System','Inspection  Cooling 4 System','Inspection  Cooling 5 System'
,'Inspection  Cooling 6 System','Inspection  Heating 1 System','Inspection  Heating 1 System Cont','Inspection  Heating 2 System','Inspection  Heating 2 System Cont'
,'Inspection  Heating 3 System','Inspection  Heating 4 System','Inspection  Heating 5 System','Inspection  Heating 6 System','Inspection 1 System','Inspection 1 System Cont'
,'Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont','Inspection 4 System','Inspection 4 System Cont','Inspection 5 System'
,'Inspection 5 System Cont','Inspection 6 System','Inspection 7 System','Inspection 8 System'
  )
),
TIME2 AS 
(
    select
   J.Job_Id
    from data_warehouse.Job J
  CROSS JOIN data_warehouse_staging.memevent ME
  LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

    where
      J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id
      and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2018-10-01' and '2019-02-28'
      and cast(ME.eventdate as date) between '2018-09-01' and '2019-01-31'
      AND T1.EventJobId is null
      and ME.eventJobNumber is null
      AND Type_id IN 
(
15830027   --Inspection - Cooling 
,15830028   --Inspection - Heating
,162318827   --Inspection (1) System
,162320866   --Inspection (2) System
,162322224   --Inspection (3) Systems
,162323246   --Inspection (4) Systems
,162323127   --Inspection (5) Systems
,162319967   --Inspection (6) Systems
,163255256   --Inspection (8) Systems
)
      and J.Status = 'Completed'
      and ME.RecurringServiceType in
  (
    'BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection  Cooling 1 System','Inspection  Cooling 1 System Cont'
,'Inspection  Cooling 2 System','Inspection  Cooling 2 System Cont','Inspection  Cooling 3 System','Inspection  Cooling 4 System','Inspection  Cooling 5 System'
,'Inspection  Cooling 6 System','Inspection  Heating 1 System','Inspection  Heating 1 System Cont','Inspection  Heating 2 System','Inspection  Heating 2 System Cont'
,'Inspection  Heating 3 System','Inspection  Heating 4 System','Inspection  Heating 5 System','Inspection  Heating 6 System','Inspection 1 System','Inspection 1 System Cont'
,'Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont','Inspection 4 System','Inspection 4 System Cont','Inspection 5 System'
,'Inspection 5 System Cont','Inspection 6 System','Inspection 7 System','Inspection 8 System'
  )
),
TIME3 AS 
(
    select
   J.Job_Id
    from data_warehouse.Job J
  CROSS JOIN data_warehouse_staging.memevent ME
  LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

    where
      J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id
      and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2019-03-01' and '2019-05-31'
      and cast(ME.eventdate as date) between '2019-02-28' and '2019-05-31'
      AND T1.EventJobId is null
      and ME.eventJobNumber is null
      AND Type_id IN 
(
15830027   --Inspection - Cooling 
,15830028   --Inspection - Heating
,162318827   --Inspection (1) System
,162320866   --Inspection (2) System
,162322224   --Inspection (3) Systems
,162323246   --Inspection (4) Systems
,162323127   --Inspection (5) Systems
,162319967   --Inspection (6) Systems
,163255256   --Inspection (8) Systems
)
      and J.Status = 'Completed'
      and ME.RecurringServiceType in
  (
    'BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection  Cooling 1 System','Inspection  Cooling 1 System Cont'
,'Inspection  Cooling 2 System','Inspection  Cooling 2 System Cont','Inspection  Cooling 3 System','Inspection  Cooling 4 System','Inspection  Cooling 5 System'
,'Inspection  Cooling 6 System','Inspection  Heating 1 System','Inspection  Heating 1 System Cont','Inspection  Heating 2 System','Inspection  Heating 2 System Cont'
,'Inspection  Heating 3 System','Inspection  Heating 4 System','Inspection  Heating 5 System','Inspection  Heating 6 System','Inspection 1 System','Inspection 1 System Cont'
,'Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont','Inspection 4 System','Inspection 4 System Cont','Inspection 5 System'
,'Inspection 5 System Cont','Inspection 6 System','Inspection 7 System','Inspection 8 System'
  )
),
TIME4 AS 
(
    select
   J.Job_Id
    from data_warehouse.Job J
  CROSS JOIN data_warehouse_staging.memevent ME
  LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

    where
      J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id
and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2018-10-01' and '2019-02-28' --4/5/2018
and cast(ME.eventdate as date) between '2018-09-01' and '2019-01-31' --11/1/2018
      AND T1.EventJobId is null
      AND ME.eventJobNumber is null
      AND Type_id IN 
(
15830030   --Inspection - Plumbing
,162319973   --Inspection (1) Water Heaters
,162322979   --Inspection (2) Water Heater
,162319364   --Inspection (3) Water Heaters
,162320882   --Inspection (4) Water Heaters

)
and J.Status = 'Completed'
and ME.RecurringServiceType in
  (
    'BOGO 1 Water Heater','BOGO 2 Water Heater','Inspection  Plumbing 1 Water Heater','Inspection  Plumbing 1 Water Heater Cont','Inspection  Plumbing 2 Water Heater'
    ,'Inspection  Plumbing 2 Water Heater Cont','Inspection  Plumbing 3 Water Heater','Inspection  Plumbing 3 Water Heater Cont','Inspection  Plumbing 4 Water Heater'
    ,'Plumbing 1 Water Heater','Plumbing 1 Water Heater Cont','Plumbing 2 Water Heater','Plumbing 2 Water Heater Cont','Plumbing 3 Water Heater'
    ,'Plumbing 3 Water Heater Cont','Plumbing 4 Water Heater'
  )
),
TIME5 AS 
(
    select distinct 
     J.Job_Id
    from data_warehouse.Job J
  CROSS JOIN data_warehouse_staging.memevent ME
  LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

    where
      J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id
and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2019-03-01' and '2019-05-31'
and cast(ME.eventdate as date) between '2019-02-01' and '2019-05-31'
      AND T1.EventJobId is null
      AND ME.eventJobNumber is null
      AND Type_id IN 
(
15830030   --Inspection - Plumbing
,162319973   --Inspection (1) Water Heaters
,162322979   --Inspection (2) Water Heater
,162319364   --Inspection (3) Water Heaters
,162320882   --Inspection (4) Water Heaters

)
and J.Status = 'Completed'
and ME.RecurringServiceType in
  (
    'BOGO 1 Water Heater','BOGO 2 Water Heater','Inspection  Plumbing 1 Water Heater','Inspection  Plumbing 1 Water Heater Cont','Inspection  Plumbing 2 Water Heater'
    ,'Inspection  Plumbing 2 Water Heater Cont','Inspection  Plumbing 3 Water Heater','Inspection  Plumbing 3 Water Heater Cont','Inspection  Plumbing 4 Water Heater'
    ,'Plumbing 1 Water Heater','Plumbing 1 Water Heater Cont','Plumbing 2 Water Heater','Plumbing 2 Water Heater Cont','Plumbing 3 Water Heater'
    ,'Plumbing 3 Water Heater Cont','Plumbing 4 Water Heater'
  )
),
alljobs as (
select *
from TIME1
UNION ALL 
select *
from TIME2
UNION ALL
select *
from TIME3
UNION ALL
select *
from TIME4
UNION ALL
select *
from TIME5
)
select * 
from alljobs