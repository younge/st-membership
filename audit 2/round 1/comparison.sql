WITH
  T1
  AS
  (
    SELECT DISTINCT EventJobId
    FROM data_warehouse_staging.memevent ME
  ),
  TIME1
  as
  (
    select
      ABS(
        DATE_DIFF ( EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago") , cast(ME.eventdate as date), DAY)
        ) 
    as days_from_job
    , ME.membershipid
    , J.Customer_Name
, J.Customer
, J.Job_Id
, J.Location_Id
, J.Job_Number
, J.Jobsite_Id
, EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) as Start_Date
, J.Duration
, J.Type_Id
, J.Type
, J.Status
, J.Modified
, ME.eventdate
, ME.eventId
, ME.RecurringServiceType
    from data_warehouse.Job J
  CROSS JOIN data_warehouse_staging.memevent ME
  LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

    where
      J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id
      /*Changed the date from 2018-03-01 to 2018-02-01. Ask RaShanda if that is ok. Seems like we should be matching jobs as soon as we turned on the system. EY 05/14/2019*/
      AND EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2018-02-01' and '2018-09-30'
      and cast(ME.eventdate as date) between '2018-02-01' and '2018-08-01'
      AND T1.EventJobId is null
      and ME.eventJobNumber is null
      AND Type_id IN 
(
15830027   --Inspection - Cooling 
,15830028   --Inspection - Heating
,162318827   --Inspection (1) System
,162320866   --Inspection (2) System
,162322224   --Inspection (3) Systems
,162323246   --Inspection (4) Systems
,162323127   --Inspection (5) Systems
,162319967   --Inspection (6) Systems
,163255256   --Inspection (8) Systems
)
      and J.Status = 'Completed'
      and ME.RecurringServiceType in
  (
    'BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection  Cooling 1 System','Inspection  Cooling 1 System Cont'
,'Inspection  Cooling 2 System','Inspection  Cooling 2 System Cont','Inspection  Cooling 3 System','Inspection  Cooling 4 System','Inspection  Cooling 5 System'
,'Inspection  Cooling 6 System','Inspection  Heating 1 System','Inspection  Heating 1 System Cont','Inspection  Heating 2 System','Inspection  Heating 2 System Cont'
,'Inspection  Heating 3 System','Inspection  Heating 4 System','Inspection  Heating 5 System','Inspection  Heating 6 System','Inspection 1 System','Inspection 1 System Cont'
,'Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont','Inspection 4 System','Inspection 4 System Cont','Inspection 5 System'
,'Inspection 5 System Cont','Inspection 6 System','Inspection 7 System','Inspection 8 System'
  )
),
TIME2 AS 
(
    select
      ABS(
        DATE_DIFF ( EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago") , cast(ME.eventdate as date), DAY)
        ) 
    as days_from_job
    , ME.membershipid
    , J.Customer_Name
, J.Customer
, J.Job_Id
, J.Location_Id
, J.Job_Number
, J.Jobsite_Id
, EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) as Start_Date
, J.Duration
, J.Type_Id
, J.Type
, J.Status
, J.Modified
, ME.eventdate
, ME.eventId
, ME.RecurringServiceType
    from data_warehouse.Job J
  CROSS JOIN data_warehouse_staging.memevent ME
  LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

    where
      J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id
      and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2018-10-01' and '2019-02-28'
      and cast(ME.eventdate as date) between '2018-09-01' and '2019-01-31'
      AND T1.EventJobId is null
      and ME.eventJobNumber is null
      AND Type_id IN 
(
15830027   --Inspection - Cooling 
,15830028   --Inspection - Heating
,162318827   --Inspection (1) System
,162320866   --Inspection (2) System
,162322224   --Inspection (3) Systems
,162323246   --Inspection (4) Systems
,162323127   --Inspection (5) Systems
,162319967   --Inspection (6) Systems
,163255256   --Inspection (8) Systems
)
      and J.Status = 'Completed'
      and ME.RecurringServiceType in
  (
    'BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection  Cooling 1 System','Inspection  Cooling 1 System Cont'
,'Inspection  Cooling 2 System','Inspection  Cooling 2 System Cont','Inspection  Cooling 3 System','Inspection  Cooling 4 System','Inspection  Cooling 5 System'
,'Inspection  Cooling 6 System','Inspection  Heating 1 System','Inspection  Heating 1 System Cont','Inspection  Heating 2 System','Inspection  Heating 2 System Cont'
,'Inspection  Heating 3 System','Inspection  Heating 4 System','Inspection  Heating 5 System','Inspection  Heating 6 System','Inspection 1 System','Inspection 1 System Cont'
,'Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont','Inspection 4 System','Inspection 4 System Cont','Inspection 5 System'
,'Inspection 5 System Cont','Inspection 6 System','Inspection 7 System','Inspection 8 System'
  )
),
TIME3 AS 
(
    select
      ABS(
        DATE_DIFF ( EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago") , cast(ME.eventdate as date), DAY)
        ) 
    as days_from_job
    , ME.membershipid
    , J.Customer_Name
, J.Customer
, J.Job_Id
, J.Location_Id
, J.Job_Number
, J.Jobsite_Id
, EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) as Start_Date
, J.Duration
, J.Type_Id
, J.Type
, J.Status
, J.Modified
, ME.eventdate
, ME.eventId
, ME.RecurringServiceType
    from data_warehouse.Job J
  CROSS JOIN data_warehouse_staging.memevent ME
  LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

    where
      J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id
      and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2019-03-01' and '2019-05-31'
      and cast(ME.eventdate as date) between '2019-02-28' and '2019-05-31'
      AND T1.EventJobId is null
      and ME.eventJobNumber is null
      AND Type_id IN 
(
15830027   --Inspection - Cooling 
,15830028   --Inspection - Heating
,162318827   --Inspection (1) System
,162320866   --Inspection (2) System
,162322224   --Inspection (3) Systems
,162323246   --Inspection (4) Systems
,162323127   --Inspection (5) Systems
,162319967   --Inspection (6) Systems
,163255256   --Inspection (8) Systems
)
      and J.Status = 'Completed'
      and ME.RecurringServiceType in
  (
    'BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection  Cooling 1 System','Inspection  Cooling 1 System Cont'
,'Inspection  Cooling 2 System','Inspection  Cooling 2 System Cont','Inspection  Cooling 3 System','Inspection  Cooling 4 System','Inspection  Cooling 5 System'
,'Inspection  Cooling 6 System','Inspection  Heating 1 System','Inspection  Heating 1 System Cont','Inspection  Heating 2 System','Inspection  Heating 2 System Cont'
,'Inspection  Heating 3 System','Inspection  Heating 4 System','Inspection  Heating 5 System','Inspection  Heating 6 System','Inspection 1 System','Inspection 1 System Cont'
,'Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont','Inspection 4 System','Inspection 4 System Cont','Inspection 5 System'
,'Inspection 5 System Cont','Inspection 6 System','Inspection 7 System','Inspection 8 System'
  )
),
TIME4 AS 
(
    select
      ABS(
        DATE_DIFF ( EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago") , cast(ME.eventdate as date), DAY)
        ) 
    as days_from_job
    , ME.membershipid
    , J.Customer_Name
, J.Customer
, J.Job_Id
, J.Location_Id
, J.Job_Number
, J.Jobsite_Id
, EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) as Start_Date
, J.Duration
, J.Type_Id
, J.Type
, J.Status
, J.Modified
, ME.eventdate
, ME.eventId
, ME.RecurringServiceType
    from data_warehouse.Job J
  CROSS JOIN data_warehouse_staging.memevent ME
  LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

    where
      J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id
and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2018-10-01' and '2019-02-28'
and cast(ME.eventdate as date) between '2018-09-01' and '2019-01-31'
      AND T1.EventJobId is null
      AND ME.eventJobNumber is null
      AND Type_id IN 
(
15830030   --Inspection - Plumbing
,162319973   --Inspection (1) Water Heaters
,162322979   --Inspection (2) Water Heater
,162319364   --Inspection (3) Water Heaters
,162320882   --Inspection (4) Water Heaters

)
and J.Status = 'Completed'
and ME.RecurringServiceType in
  (
    'BOGO 1 Water Heater','BOGO 2 Water Heater','Inspection  Plumbing 1 Water Heater','Inspection  Plumbing 1 Water Heater Cont','Inspection  Plumbing 2 Water Heater'
    ,'Inspection  Plumbing 2 Water Heater Cont','Inspection  Plumbing 3 Water Heater','Inspection  Plumbing 3 Water Heater Cont','Inspection  Plumbing 4 Water Heater'
    ,'Plumbing 1 Water Heater','Plumbing 1 Water Heater Cont','Plumbing 2 Water Heater','Plumbing 2 Water Heater Cont','Plumbing 3 Water Heater'
    ,'Plumbing 3 Water Heater Cont','Plumbing 4 Water Heater'
  )
),
TIME5 AS 
(
    select
      ABS(
        DATE_DIFF ( EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago") , cast(ME.eventdate as date), DAY)
        ) 
    as days_from_job
    , ME.membershipid
    , J.Customer_Name
, J.Customer
, J.Job_Id
, J.Location_Id
, J.Job_Number
, J.Jobsite_Id
, EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) as Start_Date
, J.Duration
, J.Type_Id
, J.Type
, J.Status
, J.Modified
, ME.eventdate
, ME.eventId
, ME.RecurringServiceType
    from data_warehouse.Job J
  CROSS JOIN data_warehouse_staging.memevent ME
  LEFT JOIN T1 ON J.Job_Id = T1.EventJobId

    where
      J.Customer = ME.Customer_Id and J.Location_Id = me.Location_Id
and EXTRACT(DATE FROM J.Start_Time AT TIME ZONE "America/Chicago" ) between '2019-03-01' and '2019-05-31'
and cast(ME.eventdate as date) between '2019-02-01' and '2019-05-31'
      AND T1.EventJobId is null
      AND ME.eventJobNumber is null
      AND Type_id IN 
(
15830030   --Inspection - Plumbing
,162319973   --Inspection (1) Water Heaters
,162322979   --Inspection (2) Water Heater
,162319364   --Inspection (3) Water Heaters
,162320882   --Inspection (4) Water Heaters

)
and J.Status = 'Completed'
and ME.RecurringServiceType in
  (
    'BOGO 1 Water Heater','BOGO 2 Water Heater','Inspection  Plumbing 1 Water Heater','Inspection  Plumbing 1 Water Heater Cont','Inspection  Plumbing 2 Water Heater'
    ,'Inspection  Plumbing 2 Water Heater Cont','Inspection  Plumbing 3 Water Heater','Inspection  Plumbing 3 Water Heater Cont','Inspection  Plumbing 4 Water Heater'
    ,'Plumbing 1 Water Heater','Plumbing 1 Water Heater Cont','Plumbing 2 Water Heater','Plumbing 2 Water Heater Cont','Plumbing 3 Water Heater'
    ,'Plumbing 3 Water Heater Cont','Plumbing 4 Water Heater'
  )
),
  ds1
  as
  (
    select *
    , row_number() over (partition by Location_Id, membershipid order by days_from_job) as row_id
    from TIME1
  ),
    ds2
  as
  (
    select *
    , row_number() over (partition by Location_Id, membershipid order by days_from_job) as row_id
    from TIME2
  ),
    ds3
  as
  (
    select *
    , row_number() over (partition by Location_Id, membershipid order by days_from_job) as row_id
    from TIME3
  ),
    ds4
  as
  (
    select *
    , row_number() over (partition by Location_Id, membershipid order by days_from_job) as row_id
    from TIME4
  ),
    ds5
  as
  (
    select *
    , row_number() over (partition by Location_Id, membershipid order by days_from_job) as row_id
    from TIME5
  )
,
example1 as 
(
select *
from ds1
where row_id = 1
UNION ALL 
select *
from ds2
where row_id = 1
UNION ALL
select *
from ds3
where row_id = 1
UNION ALL
select *
from ds4
where row_id = 1
UNION ALL
select *
from ds5
where row_id = 1
),
 jobs as (
select J.*
from data_warehouse.Job J
LEFT JOIN data_warehouse_staging.memevent ME ON J.Job_id = ME.EventJobId
WHERE me.EventJobId is null
and

Type_id IN (
15830027   --Inspection - Cooling 
,15830028   --Inspection - Heating
,162318827   --Inspection (1) System
,162320866   --Inspection (2) System
,162322224   --Inspection (3) Systems
,162323246   --Inspection (4) Systems
,162323127   --Inspection (5) Systems
,162319967   --Inspection (6) Systems
,163255256   --Inspection (8) Systems
,15830030   --Inspection - Plumbing
,162319973   --Inspection (1) Water Heaters
,162322979   --Inspection (2) Water Heater
,162319364   --Inspection (3) Water Heaters
,162320882   --Inspection (4) Water Heaters
)
AND Status = 'Completed'
AND EXTRACT(DATE FROM Start_Time AT TIME ZONE "America/Chicago" ) >= '2018-03-01'
and EXTRACT(DATE FROM Start_Time AT TIME ZONE "America/Chicago" ) <= '2019-05-13'
)
select jobs.*
from jobs
left join example1 e1 on jobs.job_id = e1.job_id
--left data_warehouse_staging.job2event j2 on j2.Job_Id = e1.Job_Id and e1.eventId = j2.EventId
where e1.job_id is null

ORDER BY Customer, Location_Id