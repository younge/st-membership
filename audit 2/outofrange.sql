WITH DS1 AS 
(select 
job2event.CustomerMembership_Id
,job2event.Customer
,job2event.Customer_Id
,job2event.Location_Id
,job2event.Job_Id
,EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") Start_time
,job2event.Name
,job2event.EventId as ST_EventID
,job2event.EventDate as ST_EventDate
,CASE
  WHEN 
    ((EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") BETWEEN '2018-02-01' AND '2018-09-30' ) AND (job2event.EventDate BETWEEN '2018-02-01' AND '2018-08-01'))  
    AND job2event.Name IN  
    ('BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection - Cooling 1 System',
    'Inspection - Cooling 1 System Cont','Inspection - Cooling 2 System','Inspection - Cooling 2 System Cont','Inspection - Cooling 3 System',
    'Inspection - Cooling 4 System','Inspection - Cooling 5 System','Inspection - Cooling 6 System','Inspection - Heating 1 System',
    'Inspection - Heating 1 System Cont','Inspection - Heating 2 System','Inspection - Heating 2 System Cont','Inspection - Heating 3 System',
    'Inspection - Heating 4 System','Inspection - Heating 5 System','Inspection - Heating 6 System','Inspection 1 System',
    'Inspection 1 System Cont','Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont',
    'Inspection 4 System','Inspection 4 System Cont','Inspection 5 System','Inspection 5 System Cont','Inspection 6 System',
    'Inspection 7 System','Inspection 8 System')
  THEN 'In Range'
  WHEN 
    ((EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") BETWEEN '2018-10-01' AND '2019-02-28' ) AND (job2event.EventDate BETWEEN '2018-09-01' AND '2019-01-31'))  
    AND job2event.Name IN  
    ('BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection - Cooling 1 System',
    'Inspection - Cooling 1 System Cont','Inspection - Cooling 2 System','Inspection - Cooling 2 System Cont','Inspection - Cooling 3 System',
    'Inspection - Cooling 4 System','Inspection - Cooling 5 System','Inspection - Cooling 6 System','Inspection - Heating 1 System',
    'Inspection - Heating 1 System Cont','Inspection - Heating 2 System','Inspection - Heating 2 System Cont','Inspection - Heating 3 System',
    'Inspection - Heating 4 System','Inspection - Heating 5 System','Inspection - Heating 6 System','Inspection 1 System',
    'Inspection 1 System Cont','Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont',
    'Inspection 4 System','Inspection 4 System Cont','Inspection 5 System','Inspection 5 System Cont','Inspection 6 System',
    'Inspection 7 System','Inspection 8 System')
  THEN 'In Range'
  WHEN 
    ((EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") BETWEEN '2019-03-01' AND '2019-06-01' ) AND (job2event.EventDate BETWEEN '2019-02-28' AND '2019-06-01'))  
    AND job2event.Name IN  
    ('BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection - Cooling 1 System',
    'Inspection - Cooling 1 System Cont','Inspection - Cooling 2 System','Inspection - Cooling 2 System Cont','Inspection - Cooling 3 System',
    'Inspection - Cooling 4 System','Inspection - Cooling 5 System','Inspection - Cooling 6 System','Inspection - Heating 1 System',
    'Inspection - Heating 1 System Cont','Inspection - Heating 2 System','Inspection - Heating 2 System Cont','Inspection - Heating 3 System',
    'Inspection - Heating 4 System','Inspection - Heating 5 System','Inspection - Heating 6 System','Inspection 1 System',
    'Inspection 1 System Cont','Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont',
    'Inspection 4 System','Inspection 4 System Cont','Inspection 5 System','Inspection 5 System Cont','Inspection 6 System',
    'Inspection 7 System','Inspection 8 System')
  THEN 'In Range'
  WHEN 
    ((EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") BETWEEN '2018-02-01' AND '2019-01-31' ) AND (job2event.EventDate BETWEEN '2018-02-01' AND '2019-01-31'))  
    AND job2event.Name IN  
    ('BOGO 1 Water Heater','BOGO 2 Water Heater','Inspection - Plumbing 1 Water Heater','Inspection - Plumbing 1 Water Heater Cont',
    'Inspection - Plumbing 2 Water Heater','Inspection - Plumbing 2 Water Heater Cont','Inspection - Plumbing 3 Water Heater',
    'Inspection - Plumbing 3 Water Heater Cont','Inspection - Plumbing 4 Water Heater','Plumbing 1 Water Heater',
    'Plumbing 1 Water Heater Cont','Plumbing 2 Water Heater','Plumbing 2 Water Heater Cont','Plumbing 3 Water Heater',
    'Plumbing 3 Water Heater Cont','Plumbing 4 Water Heater')
  THEN 'In Range'
    WHEN 
    ((EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") BETWEEN '2019-02-01' AND '2019-06-01' ) AND (job2event.EventDate BETWEEN '2019-02-01' AND '2019-06-01'))  
    AND job2event.Name IN  
    ('BOGO 1 Water Heater','BOGO 2 Water Heater','Inspection - Plumbing 1 Water Heater','Inspection - Plumbing 1 Water Heater Cont',
    'Inspection - Plumbing 2 Water Heater','Inspection - Plumbing 2 Water Heater Cont','Inspection - Plumbing 3 Water Heater',
    'Inspection - Plumbing 3 Water Heater Cont','Inspection - Plumbing 4 Water Heater','Plumbing 1 Water Heater',
    'Plumbing 1 Water Heater Cont','Plumbing 2 Water Heater','Plumbing 2 Water Heater Cont','Plumbing 3 Water Heater',
    'Plumbing 3 Water Heater Cont','Plumbing 4 Water Heater')
  THEN 'In Range'
  ELSE 'Out Of Range'
  END AS DATES
  
from data_warehouse_staging.job2event
JOIN data_warehouse.Job on Job.Job_id = job2event.Job_Id
--where job2event.CustomerMembership_Id = 175995069
)

SELECT DS1.*
FROM DS1 
JOIN data_warehouse_staging.st_has_tempo_doesnt ON DS1.Job_Id = st_has_tempo_doesnt.Job_Id
WHERE DATES = 'Out Of Range'
