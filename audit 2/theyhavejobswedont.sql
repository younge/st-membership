with ds1 as 
(
select 
tempo_has_st_doesnt.membershipid
,tempo_has_st_doesnt.Customer_Name
,tempo_has_st_doesnt.Customer
,tempo_has_st_doesnt.Location_Id
,tempo_has_st_doesnt.Job_Id
,tempo_has_st_doesnt.Start_Date
,tempo_has_st_doesnt.eventdate as Tempo_eventdate
,tempo_has_st_doesnt.eventId as Tempo_eventId
,tempo_has_st_doesnt.Type
,st_has_tempo_doesnt.EventId as ST_EventID
,st_has_tempo_doesnt.EventDate as ST_EventDate

from data_warehouse_staging.st_has_tempo_doesnt
join data_warehouse_staging.tempo_has_st_doesnt on st_has_tempo_doesnt.Job_Id = tempo_has_st_doesnt.Job_Id /*and tempo_has_st_doesnt.eventId = st_has_tempo_doesnt.EventId*/
)

select 
st_has_tempo_doesnt.CustomerMembership_Id
,st_has_tempo_doesnt.Customer
,st_has_tempo_doesnt.Customer_Id
,st_has_tempo_doesnt.Location_Id
,EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") Start_time
,st_has_tempo_doesnt.Job_Id
,st_has_tempo_doesnt.Name
,st_has_tempo_doesnt.EventId as ST_EventID
,st_has_tempo_doesnt.EventDate as ST_EventDate
,CASE
  WHEN 
    ((EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") BETWEEN '2018-02-01' AND '2018-09-30' ) AND (st_has_tempo_doesnt.EventDate BETWEEN '2018-02-01' AND '2018-08-01'))  AND st_has_tempo_doesnt.Name IN  
    ('BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection - Cooling 1 System',
    'Inspection - Cooling 1 System Cont','Inspection - Cooling 2 System','Inspection - Cooling 2 System Cont','Inspection - Cooling 3 System',
    'Inspection - Cooling 4 System','Inspection - Cooling 5 System','Inspection - Cooling 6 System','Inspection - Heating 1 System',
    'Inspection - Heating 1 System Cont','Inspection - Heating 2 System','Inspection - Heating 2 System Cont','Inspection - Heating 3 System',
    'Inspection - Heating 4 System','Inspection - Heating 5 System','Inspection - Heating 6 System','Inspection 1 System',
    'Inspection 1 System Cont','Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont',
    'Inspection 4 System','Inspection 4 System Cont','Inspection 5 System','Inspection 5 System Cont','Inspection 6 System',
    'Inspection 7 System','Inspection 8 System')
  THEN 'In Range'
  WHEN 
    ((EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") BETWEEN '2018-10-01' AND '2019-02-28' ) AND (st_has_tempo_doesnt.EventDate BETWEEN '2018-09-01' AND '2019-01-31'))  AND st_has_tempo_doesnt.Name IN  
    ('BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection - Cooling 1 System',
    'Inspection - Cooling 1 System Cont','Inspection - Cooling 2 System','Inspection - Cooling 2 System Cont','Inspection - Cooling 3 System',
    'Inspection - Cooling 4 System','Inspection - Cooling 5 System','Inspection - Cooling 6 System','Inspection - Heating 1 System',
    'Inspection - Heating 1 System Cont','Inspection - Heating 2 System','Inspection - Heating 2 System Cont','Inspection - Heating 3 System',
    'Inspection - Heating 4 System','Inspection - Heating 5 System','Inspection - Heating 6 System','Inspection 1 System',
    'Inspection 1 System Cont','Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont',
    'Inspection 4 System','Inspection 4 System Cont','Inspection 5 System','Inspection 5 System Cont','Inspection 6 System',
    'Inspection 7 System','Inspection 8 System')
  THEN 'In Range'
  WHEN 
    ((EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") BETWEEN '2019-03-01' AND '2019-06-01' ) AND (st_has_tempo_doesnt.EventDate BETWEEN '2019-02-28' AND '2019-06-01'))  AND st_has_tempo_doesnt.Name IN  
    ('BOGO Inspection 1 System','BOGO Inspection 2 System','BOGO Inspection 3 System','BOGO Inspection 4 System','Inspection - Cooling 1 System',
    'Inspection - Cooling 1 System Cont','Inspection - Cooling 2 System','Inspection - Cooling 2 System Cont','Inspection - Cooling 3 System',
    'Inspection - Cooling 4 System','Inspection - Cooling 5 System','Inspection - Cooling 6 System','Inspection - Heating 1 System',
    'Inspection - Heating 1 System Cont','Inspection - Heating 2 System','Inspection - Heating 2 System Cont','Inspection - Heating 3 System',
    'Inspection - Heating 4 System','Inspection - Heating 5 System','Inspection - Heating 6 System','Inspection 1 System',
    'Inspection 1 System Cont','Inspection 2 System','Inspection 2 System Cont','Inspection 3 System','Inspection 3 System Cont',
    'Inspection 4 System','Inspection 4 System Cont','Inspection 5 System','Inspection 5 System Cont','Inspection 6 System',
    'Inspection 7 System','Inspection 8 System')
  THEN 'In Range'
  WHEN 
    ((EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") BETWEEN '2018-02-01' AND '2019-01-31' ) AND (st_has_tempo_doesnt.EventDate BETWEEN '2018-02-01' AND '2019-01-31'))  
    AND st_has_tempo_doesnt.Name IN  
    ('BOGO 1 Water Heater','BOGO 2 Water Heater','Inspection - Plumbing 1 Water Heater','Inspection - Plumbing 1 Water Heater Cont',
    'Inspection - Plumbing 2 Water Heater','Inspection - Plumbing 2 Water Heater Cont','Inspection - Plumbing 3 Water Heater',
    'Inspection - Plumbing 3 Water Heater Cont','Inspection - Plumbing 4 Water Heater','Plumbing 1 Water Heater',
    'Plumbing 1 Water Heater Cont','Plumbing 2 Water Heater','Plumbing 2 Water Heater Cont','Plumbing 3 Water Heater',
    'Plumbing 3 Water Heater Cont','Plumbing 4 Water Heater')
  THEN 'In Range'
    WHEN 
    ((EXTRACT(DATE FROM Job.Start_time AT TIME ZONE "America/Chicago") BETWEEN '2019-02-01' AND '2019-06-01' ) AND (st_has_tempo_doesnt.EventDate BETWEEN '2019-02-01' AND '2019-06-01'))  AND Name IN  
    ('BOGO 1 Water Heater','BOGO 2 Water Heater','Inspection - Plumbing 1 Water Heater','Inspection - Plumbing 1 Water Heater Cont',
    'Inspection - Plumbing 2 Water Heater','Inspection - Plumbing 2 Water Heater Cont','Inspection - Plumbing 3 Water Heater',
    'Inspection - Plumbing 3 Water Heater Cont','Inspection - Plumbing 4 Water Heater','Plumbing 1 Water Heater',
    'Plumbing 1 Water Heater Cont','Plumbing 2 Water Heater','Plumbing 2 Water Heater Cont','Plumbing 3 Water Heater',
    'Plumbing 3 Water Heater Cont','Plumbing 4 Water Heater')
  THEN 'In Range'
  ELSE 'Out Of Range'
  END AS DATES

from data_warehouse_staging.st_has_tempo_doesnt
left join ds1 on st_has_tempo_doesnt.Job_Id = ds1.Job_Id
JOIN data_warehouse.Job on Job.Job_id = st_has_tempo_doesnt.Job_Id
where ds1.Job_Id is null


